﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace LunchMenu.Tests.DbEntities
{
    [TestClass]
    public class DbEntitiesTests : BaseDbTests
    {

        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            BaseDbTests.ClassInitialize(context);
        }


        [TestMethod]
        public void When_Add_Menu_With_Dishes_It_Should_Work()
        {

            var dishes = GenerateDishes();
            var menu = new Menu() { Date = DateTime.Today, IsActive = true, ProviderId = 1, Dishes = dishes };

            dbContext.Menus.Add(menu);
            dbContext.SaveChanges();

            Assert.AreEqual(1, dbContext.Menus.Count(x => (x.Date == DateTime.Today) && (x.IsActive == true) && (x.ProviderId == 1)));
            Assert.AreEqual(dishes.Count, dbContext.Menus.FirstOrDefault(x => x.Id==menu.Id).Dishes.Count());
        }

        [TestMethod]
        public void When_Delete_Menu_Then_Dishes_Should_Be_Deleted()
        {
            var expectedCount = dbContext.Dishes.Count();

            var dishes = GenerateDishes();
            var menu = new Menu() { Date = DateTime.Today, IsActive = true, ProviderId = 1, Dishes = dishes };

            dbContext.Menus.Add(menu);
            dbContext.SaveChanges();
            
            Assert.AreNotEqual(expectedCount, dbContext.Dishes.Count());

            dbContext.Menus.Remove(menu);
            dbContext.SaveChanges();

            Assert.AreEqual(expectedCount, dbContext.Dishes.Count());
        }

    }

}
