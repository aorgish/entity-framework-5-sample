﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;

namespace LunchMenu.Tests.DbEntities
{
    public class LunchMenuContext : DbContext
    {
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Dish> Dishes { get; set; }

        public DbSet<Menu> Menus { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDish> OrderDishes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Location> Locations { get; set; }

        public LunchMenuContext(string name) : base(name) { }
    }


    public class Provider
    {
        [Key]           
        public int Id { get; set; }
        
        [MaxLength(20)] 
        public string ProviderName { get; set; }
        
        public virtual ICollection<Location> Location { get; set; }
    }

    public class Dish
    {
        [Key]           
        public int Id { get; set; }
        
        [MaxLength(50)] 
        public string Title { get; set; }
        
        [MaxLength(50)] 
        public string Category { get; set; }
        
        [MaxLength(50)]
        public string Details { get; set; }
        
        [MaxLength(50)] 
        public string Weight { get; set; }
        
        public int Price { get; set; }
        public int ExcelRow { get; set; }

        public virtual Menu Menu { get; set; }
        public int MenuId { get; set; }
    }

    public class Menu
    {
        [Key]          
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public bool IsActive { get; set; }

        public virtual Provider Provider { get; set; }
        public int ProviderId { get; set; }
        public virtual ICollection<Dish> Dishes { get; set; }
    }


    public class Order
    {
        [Key]
        public int Id { get; set; }

        public virtual User User { get; set; }
        public string UserId { get; set; }

        public int LocationId { get; set; }
        public Location Location { get; set; }

        public DateTime OrderDate { get; set; }
        public int Total { get; set; }
        public virtual ICollection<OrderDish> OrderDishes { get; set; }
    }

    public class OrderDish
    {
        [Key]
        public int Id { get; set; }

        public virtual Order Order { get; set; }
        public int OrderId { get; set; }

        public virtual Dish Dish { get; set; }
        public int DishId { get; set; }

        public int Count { get; set; }
    }

    public class Location
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(40)]
        public string Name { get; set; }

        [MaxLength(50)]
        public string DeliveryAddress { get; set; }

        [MaxLength(15)]
        public string DeliveryTime { get; set; }   // TimeSpan string 
    }

    public class User
    {
        [Key]
        [MaxLength(20)]
        public string Id { get; set; }

        [MaxLength(50)]
        public string UserName { get; set; }

        [MaxLength(60)]
        public string EMail { get; set; }
    }



}
