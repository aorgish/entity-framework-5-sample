﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace LunchMenu.Tests.DbEntities
{
    [TestClass]
    public class BaseDbTests
    {
        static protected string dbBackupFileName = @"TestDb.sdf";
        static protected int dbCounter = 0;

        static protected string LocationName1 = "City1, Location1,1";
        static protected string LocationName2 = "City2, Location2,2";
        static protected string ProviderName1 = "Provider1";
        static protected string ProviderName2 = "Provider2";
        static protected string UserName1 = "UserName1";
        static protected string UserName2 = "UserName2";

        protected LunchMenuContext dbContext;
        protected string dbFileName;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            if (File.Exists(dbBackupFileName)) return;

            using (var backupContext = new LunchMenuContext(string.Format("Data Source={0}", dbBackupFileName)))
            {
                InitializeDbData(backupContext);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            dbFileName = string.Format("Test{0}.sdf", dbCounter++);
            File.Copy(dbBackupFileName, dbFileName);
            dbContext = new LunchMenuContext(string.Format("Data Source={0}", dbFileName));
        }

        [TestCleanup]
        public void TestCleanup()
        {
            dbContext.Dispose();
            File.Delete(dbFileName);
        }


        protected static void InitializeDbData(LunchMenuContext dbContext)
        {
            var location1 = new Location { Id = 1, Name = LocationName1, DeliveryAddress = "234, Noname st, City1",        DeliveryTime = TimeSpan.FromHours(13).ToString() };
            var location2 = new Location { Id = 2, Name = LocationName2, DeliveryAddress = "567, Default st, Default",     DeliveryTime = TimeSpan.FromHours(12).ToString() };
            dbContext.Locations.Add(location1);
            dbContext.Locations.Add(location2);
            dbContext.SaveChanges();

            var provider1 = new Provider { Id = 1, ProviderName = ProviderName1, Location = new List<Location>() { location1, location2 } };
            var provider2 = new Provider { Id = 2, ProviderName = ProviderName2, Location = new List<Location>() { location1 } };
            dbContext.Providers.Add(provider1);
            dbContext.Providers.Add(provider2);
            dbContext.SaveChanges();

            var user1 = new User() { Id = "1111111111111111111", EMail = UserName1 + "@mail.com", UserName = UserName1 };
            var user2 = new User() { Id = "2222222222222222222", EMail = UserName2 + "@mail.com", UserName = UserName2 };
            dbContext.Users.Add(user1);
            dbContext.Users.Add(user2);
            dbContext.SaveChanges();
        }


        protected ICollection<Dish> GenerateDishes()
        {
            return new List<Dish>() {
                           new Dish { Category = "category1", Details = "details1", Price = 100, ExcelRow = 0, Title = "Dish1", Weight = "100" },
                           new Dish { Category = "category1", Details = "details2", Price = 100, ExcelRow = 1, Title = "Dish2", Weight = "100" },
                           new Dish { Category = "category1", Details = "details3", Price = 100, ExcelRow = 2, Title = "Dish3", Weight = "100" },
                           new Dish { Category = "category2", Details = "details4", Price = 100, ExcelRow = 3, Title = "Dish4", Weight = "100" },
                           new Dish { Category = "category2", Details = "details5", Price = 100, ExcelRow = 4, Title = "Dish5", Weight = "100" },
                       };
        }

    }

}
